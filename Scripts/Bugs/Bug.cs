﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Bug : MonoBehaviour {


    [SerializeField]
	private float speed;

	private float currentSpeed;

	private float slowSpeed;

	[SerializeField]
	private GameObject slowPrefab;

	[SerializeField]
	private GameObject eyesPrefab;

	[SerializeField]
	private GameObject bodyPrefab;

	private GameObject slowInstance;


	[SerializeField]
	private PathSolver pathFinder;

	[SerializeField]
	private Animator animator;

    [SerializeField]
    private Animator frontLegs;
    [SerializeField]
    private Animator midLegs;
    [SerializeField]
    private Animator backLegs;

    private int score;

	private double score2;


	private Queue<Stack<Node>> moveScheme;
    private Stack<Node> path;

	Vector3 adjuster;

    public Point gridPos { get; set; }

    public bool britRunner { get; set; }

    public Brit britToNotify;

    private Vector3 destination;

	private bool done;

	private bool startedAnimation;
	private bool buttonsActivated;
	private bool clickAbsorbed;

	private bool slowed;
	private double slowDurationLeft;


	private float overFlow;

    private bool notifiedBait;

    private void Move()
    {


		if (slowed) {

			slowDurationLeft -= speed * Time.deltaTime;
		
			if (slowDurationLeft <= 0) {
				slowed = false;
				slowPrefab.GetComponent<SpriteRenderer> ().enabled = false;
				currentSpeed = speed;

				setAnimator ();
			}
		
		}




		float potentialOverflow = currentSpeed * Time.deltaTime + overFlow - Vector3.Distance (transform.position, destination + adjuster);



		transform.position = Vector3.MoveTowards (transform.position, destination + adjuster, currentSpeed * Time.deltaTime);
		//transform.position = Vector3.Lerp (transform.position, destination + adjuster, currentSpeed * Time.deltaTime);





		score2 += Time.deltaTime;
		loadLevel.Instance.updateCurrentScore (score2);

		if(transform.position == destination + adjuster)
        {
			//Debug.Log (currentSpeed * Time.deltaTime + "the normalspeed");
			//Debug.Log (overFlow + "the overflow");
			overFlow = potentialOverflow;

			if (path.Count > 0) {
				gridPos = path.Peek ().GridPosition;
				destination = path.Pop ().WorldPosition;
				if (path.Count < 4 && !notifiedBait) {
					loadLevel.Instance.notifyBait ();
                    notifiedBait = true;
				}
				setDepht (gridPos.Y);
				Vector2 direction = (destination + adjuster) - transform.position;
				float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
				Quaternion rotation = Quaternion.AngleAxis (angle - 90, Vector3.forward);
				transform.rotation = Quaternion.Slerp (transform.rotation, rotation, 1);
			} else if (moveScheme.Count > 0) {

				SetPath (moveScheme.Dequeue ());

			} else if (!done) {
				score += 1;
				//loadLevel.Instance.updateCurrentScore (score);
				setDepht (gridPos.Y);
				destination = destination + new Vector3 (2.2f, 0, 0);
				done = true;
				Vector2 direction = (destination + adjuster) - transform.position;
				float angle = Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg;
				Quaternion rotation = Quaternion.AngleAxis (angle - 90, Vector3.forward);
				transform.rotation = Quaternion.Slerp (transform.rotation, rotation, 1);
			} else if (!startedAnimation) {

                if (britRunner) {
                    GameManager.Instance.runnerDone();
                    GameObject.FindGameObjectWithTag("brit").GetComponent<Brit>().bugDone = true;
                    GameManager.Instance.waitingDone();
                    loadLevel.Instance.clearStopButtonSwitch();
                    loadLevel.Instance.resetPoof();
                    Destroy(gameObject);


                }
                else
                {
                    bool failed = false;
                    GameManager.Instance.runnerDone();
                    int currentLevel;
                    score2 = Math.Round(score2, 3); ;
                    if (Int32.TryParse((LevelManager.Instance.chosenLevel), out currentLevel))
                    {
                       
                        currentLevel -= 1;
                        int starsToReceive = 0;

                        if (score2 >= loadLevel.Instance.getScoringData(2))
                        {
                            starsToReceive = 3;
                            playWinAnimation(3);

                        }
                        else if (score2 >= loadLevel.Instance.getScoringData(1))
                        {
                            starsToReceive = 2;
                            playWinAnimation(2);

                        }
                        else if (score2 >= loadLevel.Instance.getScoringData(0))
                        {
                            starsToReceive = 1;
                            playWinAnimation(1);

                        }
                        else
                        {
                            failed = true;
                            playWinAnimation(0);

                        }

                       
                        if (SaveLoad.save.getAct((LevelManager.Instance.pickedAct.idCode)).checkLevelProgress(currentLevel)<starsToReceive)
                            //if (SaveLoad.save.getAct(LevelManager.Instance.pickedAct.idCode).checkBest(currentLevel, score2))
                        {
                            SaveLoad.save.getAct(LevelManager.Instance.pickedAct.idCode).setLevelProgress(currentLevel, starsToReceive);
                            SaveLoad.save.getAct(LevelManager.Instance.pickedAct.idCode).updateAct();
                           
                        }
                        SaveLoad.save.addToWaited(score2);
                        
                        SaveLoad.Save();
                        

                    }
                    else
                    {

                        if (score2 >= 5)
                        {
                            playWinAnimation(3);
                        }

                        else if (score2 >= 4)
                        {
                            playWinAnimation(2);
                        }

                        else if (score2 >= 3)
                        {
                            playWinAnimation(1);
                        }

                        else
                        {
                            playWinAnimation(0);
                        }


                    }

                    startedAnimation = true;
                    loadLevel.Instance.showFinishButtons(true, failed);
                }
					
			}

        }
            
            
            
    }


	private void playWinAnimation(int stars){
	
		GameObject.FindGameObjectWithTag ("winanimations").GetComponent<winAnimations>().playAnimation(stars);

	}

	private void stopAnimation(){

		GameObject.FindGameObjectWithTag ("winanimations").GetComponent<winAnimations> ().stopAnimation ();
	
	}

	public float Speed {
		get {
			return speed;
		}
	}

    public void Spawn()
    {	
		adjuster = new Vector3 (0.5f, -0.5f, 0);

		overFlow = 0;

		Vector3 spawnTest = new Vector3 (-1.1f, 0, 0);

		currentSpeed = speed;

		slowSpeed = speed / 2;
		score = 0;
		score2 = 0;
		loadLevel.Instance.updateCurrentScore (score2);

		setMoveScheme ();
		SetPath (moveScheme.Dequeue());


		setDepht (loadLevel.Instance.startPos.Y);

        float scaleValueIndex = 9f/loadLevel.Instance.rowCount;

      //  SetPath(PathFinder.pathWay);
		transform.position = loadLevel.Instance.Tiles[loadLevel.Instance.startPos].transform.position + adjuster + spawnTest;
        transform.localScale = Vector3.Scale(transform.localScale, new Vector3(1.5f*scaleValueIndex,1.5f*scaleValueIndex,1));
		loadLevel.Instance.ActivateBait ();

		setAnimator ();



    }

	public void cancelEarly(){
	
		GameManager.Instance.triggerStop ();
		loadLevel.Instance.resetPoof ();
		Destroy (gameObject);
	
	
	}






	private void setAnimator(){

        //animator.speed = currentSpeed / 3f;
        //backLegs.speed = currentSpeed / 3f;
        //midLegs.speed = currentSpeed / 5f;
        //frontLegs.speed = currentSpeed / 3f;

        animator.speed = currentSpeed / 4f;
        backLegs.speed = currentSpeed / 4f;
        midLegs.speed = currentSpeed / 8f;
        frontLegs.speed = currentSpeed / 4f;



    }

	public void sneezeSlow (double duration)
	{
		slowed = true;
		slowDurationLeft = duration;
		currentSpeed = slowSpeed;
		slowPrefab.GetComponent<SpriteRenderer> ().enabled = true;
		setAnimator ();
	}

	private void setMoveScheme (){
	
	
		this.moveScheme = PathFinder.moveScheme;
	
	}


	private void setDepht(int row){
		int value = (row)*10;
	
		GetComponent<SpriteRenderer> ().sortingOrder = value;
		slowPrefab.GetComponent<SpriteRenderer> ().sortingOrder = value + 1;
		backLegs.GetComponent<SpriteRenderer> ().sortingOrder = value;
		midLegs.GetComponent<SpriteRenderer> ().sortingOrder = value;
		frontLegs.GetComponent<SpriteRenderer> ().sortingOrder = value;
		bodyPrefab.GetComponent<SpriteRenderer> ().sortingOrder = value;
		eyesPrefab.GetComponent<SpriteRenderer> ().sortingOrder = value + 1;


	
	}


    private void SetPath(Stack<Node> newPath)
    {

        if (newPath != null)
        {

			if (gridPos != null) {
			
				setDepht (gridPos.Y);
			
			}
            this.path = newPath;
            this.gridPos = newPath.Peek().GridPosition;
            this.destination = newPath.Pop().WorldPosition;

        }

    }



	public void retryLevel(){
		stopAnimation ();
		GameManager.Instance.waitingDone();
		loadLevel.Instance.clearStopButtonSwitch ();		
		loadLevel.Instance.resetPoof ();
		Destroy (gameObject);
	}




	// Use this for initialization
	// Update is called once per frame
	void FixedUpdate () {

		if (GameManager.Instance.running) {
			Move ();
		}
	}
}
