﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary; 
using UnityEngine;
using System.IO;

public static class SaveLoad{

	public static PlayerStats save = new PlayerStats();


	public static void Save(){
		//save = PlayerStats.current;
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd");
		bf.Serialize (file, SaveLoad.save);
		file.Close ();
	}

	public static void Load(){
	
		if (File.Exists (Application.persistentDataPath + "/savedGames.gd")) {
		
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
			SaveLoad.save = (PlayerStats)bf.Deserialize (file);
			file.Close ();
		
		}
	
	
	}


}
