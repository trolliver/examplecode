﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerStats{

	public static PlayerStats current;

	Dictionary<string, Act> acts;
    HashSet<string> boughtActs;
    int totalStars;
	private bool hasBought;

    private double timeWaited;

	public PlayerStats(){
		acts = new Dictionary<string, Act>();
		totalStars = 0;
	}

    public bool adFree()
    {
        bool adFree;
        Debug.Log(boughtActs + " what is here?");
        if (boughtActs == null) adFree = false;
        else adFree = true;
        return adFree;
    }

    public Act getAct (string key){
	
		if (acts.ContainsKey(key)) {
			return acts [key];
		} else {

			Act newAct = new Act ();

			acts.Add (key, newAct);

			return newAct;
		}

	
	}

	public bool checkIfBoughtAct(string key){

        if (boughtActs == null) { return false; }
        else {
            return boughtActs.Contains(key);
        }
	
	}

    public void addToBought(string actCode) {
        if (boughtActs == null) {
            boughtActs = new HashSet<string>();
        }

        boughtActs.Add(actCode);
    }


	public bool unlocked(string key){
	
		return acts [key].Unlocked;

	}

	public int updateStars(){
		int stars = 0;
		foreach (var entry in acts) {
			stars += entry.Value.updateStars ();
		}
		totalStars = stars;
		return stars;

	}

    public void addToWaited(double time) {
        timeWaited += time;
    }

    public double presentTimeWaited() {
        return timeWaited;
    }

}
