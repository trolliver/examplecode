﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System. Serializable]
public class Level{

	private bool unlocked;
	private double bestScore;
	private int stars;

	public int Stars {
		get {
			return stars;
		}
		set {
			stars = value;
		}
	}

	public Level (){
		bestScore = 0;
		stars = 0;
		unlocked = false;
	}

	public void unlock(){
		unlocked = true;
	}
		
	public bool isUnlocked(){
		return unlocked;
	}
		
	public double BestScore {
		get {
			return bestScore;
		}
		set{
			bestScore = value;
		}
	}
}
