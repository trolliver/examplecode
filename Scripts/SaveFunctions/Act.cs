﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System. Serializable]
public class Act{

	Level[] levels;
	TutLevel[] tutLevels;
	int totalStars;
	bool unlocked;
	int[] stageRequirements = new int[3]{10, 20, 30};

	bool cleared;

	public Act(){
		unlocked = false;
		levels = new Level[20];
		tutLevels = new TutLevel[4];
		for (int i = 0; i < 4; i++) {
			tutLevels [i] = new TutLevel ();

		}
		for (int i = 0; i < 20; i++) {
			levels [i] = new Level ();
			if (i < 5) {
				unlockLevel (i);
			}
		}
	}

	public bool Unlocked {
		get {
			return unlocked;
		}
		set {
			unlocked = value;
		}
	}


	public void updateAct(){
		updateStars ();

		for (int i = 0; i < 3; i++) {
			if (totalStars >= stageRequirements [i]) {
				for(int j = 0; j < 5; j++)
				{
					unlockLevel (j+(i+1)*5);
					
				}
			}
		}




	}

	public bool Cleared {
		get {
			return cleared;
		}
	}

	public int updateStars(){
		int stars = 0;
		foreach(Level lvl in levels)
		{
			stars += lvl.Stars;
		}
		totalStars = stars;
		return stars;
	}
		

	public void playTutLevel(int i){
		tutLevels [i].setPlayed ();
	}

	public bool hasPlayedTutLevel(int i){
		return tutLevels [i].isPlayed ();
	}


	public void unlockLevel(int i){
		levels [i].unlock();

	}

	public bool checkLevelLock(int i){

		return levels [i].isUnlocked();
	
	}

	public int checkLevelProgress(int i){

		return levels [i].Stars;
	
	}

	public void setLevelProgress(int i, int stars){

		levels [i].Stars = stars;

	}

	public bool checkBest(int i, double score){
	
		if (levels [i].BestScore < score) {
			levels [i].BestScore = score;
			return true;
		}

		return false;
	
	
	}

	public double checkBest(int i){
	
		return levels [i].BestScore;
	
	}
	
	


}
