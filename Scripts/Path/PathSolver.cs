﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PathSolver: MonoBehaviour
{

	private Dictionary<Point, Node> nodeList;

	public Stack<Node> pathWay;

	public Queue<Stack<Node>> moveScheme { get; private set; }

	private void CreateNodes ()
	{
		nodeList = new Dictionary<Point, Node> ();

		foreach (Tile tile in loadLevel.Instance.Tiles.Values)
			nodeList.Add (tile.GridPos, new Node (tile));
	}

	public bool GetPath ()
	{

		moveScheme = new Queue<Stack<Node>> ();
		loadLevel level = loadLevel.Instance;

		//if no bait go from start to Goal
		if (!level.hasBait && !level.hasBait2) {
			return Solve (level.startPos, level.finishPos);
		}
		//If bait 1 go to bait 1 then goal
		else if (level.hasBait && !level.hasBait2) {

			if (Solve (level.startPos, level.baitPos)) {

				return Solve (level.baitPos, level.finishPos);


			}





		}

		//if bait 1 and 2 go to bait 1 then 2 then goal
		else if (level.hasBait && level.hasBait2) {


			if (Solve (level.startPos, level.baitPos)) {

				if (Solve (level.baitPos, level.bait2Pos)) {

					return Solve (level.bait2Pos, level.finishPos);
				}

			}

		} else if (!level.hasBait && level.hasBait2) {


			if (Solve (level.startPos, level.bait2Pos)) {

				return Solve (level.bait2Pos, level.finishPos);

			}
		}


		//if bait 2 go to bait2 then to goal

		return false;
	}

	



	public bool Solve (Point start, Point finish)
	{
		CreateNodes ();
		bool solveable = false;

		HashSet<Node> visited = new HashSet<Node> ();

		Queue<Node> nodesToTry = new Queue<Node> ();

		Stack<Node> shortestPath = new Stack<Node> ();

		Node currentNode = nodeList [start];

		visited.Add (currentNode);
		nodesToTry.Enqueue (currentNode);


		while (nodesToTry.Count != 0) {
			currentNode = nodesToTry.Dequeue ();
			if (currentNode.GridPosition == finish) {
				nodesToTry.Clear ();
				shortestPath.Push (currentNode);
				while (currentNode.previousNode != null) {
					shortestPath.Push (currentNode.previousNode);
					currentNode = currentNode.previousNode;
				}
				moveScheme.Enqueue (shortestPath);
				solveable = true;
			} else {

				for (int x = -1; x <= 1; x++) {
					for (int y = -1; y <= 1; y++) {

						if (Math.Abs (y) != Math.Abs (x)) {
							int newX = currentNode.GridPosition.X + x;
							int newY = currentNode.GridPosition.Y + y;

							//Debug.Log ("COLCOUNT: " + loadLevel.Instance.colCount );
							//Debug.Log ("ROWCOUNT: " + loadLevel.Instance.rowCount );

							if (newX >= 0 && newX < loadLevel.Instance.colCount && newY >= 0 && newY < loadLevel.Instance.rowCount) {
								Point gridStep = new Point (newX, newY);
								Node neighborNode = nodeList [gridStep];

								if (!visited.Contains (neighborNode) && neighborNode.TileRef.isPassable) {
									visited.Add (neighborNode);
									neighborNode.previousNode = currentNode;
									nodesToTry.Enqueue (neighborNode);

								}


							}



						}
					}
				}

			}




		}


		return solveable;
	}


}
