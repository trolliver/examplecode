﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    public Node previousNode;

    public Point GridPosition { get; set; }

    public Tile TileRef { get; private set; }

    public Vector3 WorldPosition { get; set; }

    public Node(Tile tileRef){

        this.TileRef = tileRef;
        this.GridPosition = tileRef.GridPos;
        this.WorldPosition = tileRef.worldPos;

    }


    public Node(Tile tileRef, Node prev)
    {
        this.previousNode = prev;
        this.TileRef = tileRef;
        this.GridPosition = tileRef.GridPos;

    }

    public override int GetHashCode()
    {
        int hash = 7;
        hash = 71 * hash + this.GridPosition.X;
        hash = 71 * hash + this.GridPosition.Y;
        return hash;
    }




}
