﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PathFinder
{

	public static String level = "1";


	private static Dictionary<Point, Node> nodeList;

	public static Stack<Node> pathWay;

	public static Queue<Stack<Node>> moveScheme { get; private set; }

	private static void CreateNodes ()
	{
		nodeList = new Dictionary<Point, Node> ();

		foreach (Tile tile in loadLevel.Instance.Tiles.Values) {

			nodeList.Add (tile.GridPos, new Node (tile));

		}

	}

	public static bool GetPath ()
	{

		resetPoof ();

		moveScheme = new Queue<Stack<Node>> ();
		loadLevel level = loadLevel.Instance;

		//if no bait go from start to Goal
		if (!level.hasBait && !level.hasBait2) {
		
		
			return Solve (level.startPos, level.finishPos);

		
		}
		//If bait 1 go to bait 1 then goal
		else if (level.hasBait && !level.hasBait2) {


			if (Solve (level.startPos, level.baitPos)) {
				poof ();
				return Solve (level.baitPos, level.finishPos);


			}
				



		}

		//if bait 1 and 2 go to bait 1 then 2 then goal
		else if (level.hasBait && level.hasBait2) {
			

			if (Solve (level.startPos, level.baitPos)) {

				poof ();


				if (Solve (level.baitPos, level.bait2Pos)) {

					poof ();

					return Solve (level.bait2Pos, level.finishPos);
				}
					
			}
				
		} else if (!level.hasBait && level.hasBait2) {
			
		
			if (Solve (level.startPos, level.bait2Pos)) {

				poof ();

				return Solve (level.bait2Pos, level.finishPos);
			
			}
		}


		//if bait 2 go to bait2 then to goal

		return false;
	}


	public static bool Solve (Point start, Point finish)
	{
		CreateNodes ();
		bool solveable = false;

		HashSet<Node> visited = new HashSet<Node> ();

		Queue<Node> nodesToTry = new Queue<Node> ();

		Stack<Node> shortestPath = new Stack<Node> ();

		Node currentNode = nodeList [start];

		visited.Add (currentNode);
		nodesToTry.Enqueue (currentNode);


		while (nodesToTry.Count != 0) {
			currentNode = nodesToTry.Dequeue ();

			if (currentNode.GridPosition == finish) {
               
				nodesToTry.Clear ();

				shortestPath.Push (currentNode);

				while (currentNode.previousNode != null) {

					//Debug.Log(currentNode.GridPosition.X + "  " + currentNode.GridPosition.Y );

					shortestPath.Push (currentNode.previousNode);
					currentNode = currentNode.previousNode;

                   
				}						
              
				moveScheme.Enqueue (shortestPath);								            
				solveable = true;
			} else {
				for (int x = -1; x <= 1; x++) {
					for (int y = -1; y <= 1; y++) {

						if (!(Math.Abs (x) == Math.Abs (y))) {
							Point gridStep = new Point (currentNode.GridPosition.X + x, currentNode.GridPosition.Y + y);

							if (loadLevel.Instance.InBounds (gridStep)) {                               
								Node neighborNode = nodeList [gridStep];

								if (!visited.Contains (neighborNode) && neighborNode.TileRef.isPassable) {
									visited.Add (neighborNode);
									neighborNode.previousNode = currentNode;
									nodesToTry.Enqueue (neighborNode);									                            
								}
							}

						}

                        
					}
				}
				for (int x = -1; x <= 1; x++) {
					for (int y = -1; y <= 1; y++) {

						if ((Math.Abs (x) == Math.Abs (y))) {
							bool allowed = true;
							Point gridStep = new Point (currentNode.GridPosition.X + x, currentNode.GridPosition.Y + y);


							if (loadLevel.Instance.InBounds (gridStep)) {                               
								Node neighborNode = nodeList [gridStep];
								if (Math.Abs (x) == Math.Abs (y)) {
									allowed = ConnectedDiagonally (currentNode, neighborNode);
								}
								if (!visited.Contains (neighborNode) && neighborNode.TileRef.isPassable && allowed) {
									visited.Add (neighborNode);
									neighborNode.previousNode = currentNode;
									nodesToTry.Enqueue (neighborNode);									                            
								}
							}

						}
					}
				}

            


			}



		}
		return solveable;
	}

	private static bool ConnectedDiagonally (Node currentNode, Node neighborNode)
	{

		Point direction = neighborNode.GridPosition - currentNode.GridPosition;

		Point first = new Point (currentNode.GridPosition.X + direction.X, currentNode.GridPosition.Y);

		Point second = new Point (currentNode.GridPosition.X, currentNode.GridPosition.Y + direction.Y);

		if (loadLevel.Instance.InBounds (first)) {
		
			if (!nodeList [first].TileRef.isPassable)
				return false;

		}

		if (loadLevel.Instance.InBounds (second)) {

			if (!nodeList [second].TileRef.isPassable)
				return false;

		}



		return true;
	}

	private static void poof(){




		foreach (Point gridPos in loadLevel.Instance.getPoofers()) {

			loadLevel.Instance.Tiles[gridPos].gameObject.GetComponentInChildren<PoofAble> ().pathPoof();

		}



	
	
	}


	private static void resetPoof(){

		foreach (Point gridPos in loadLevel.Instance.getPoofers()) {
			loadLevel.Instance.Tiles[gridPos].gameObject.GetComponentInChildren<PoofAble> ().reset ();
						

		}

	


	}



}
