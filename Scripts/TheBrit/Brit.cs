﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class Brit : MonoBehaviour
{


    [SerializeField]
    private GameObject textBox;

    [SerializeField]
    private GameObject clickBlocker;

    [SerializeField]
    private GameObject presentBox;

    [SerializeField]
    private SpriteRenderer equippedItem;

    [SerializeField]
    private Sprite[] itemsToEquip;

    [SerializeField]
    private GameDictionary dictionary;

    private Dictionary<char, GameObject> ShroomList;

    private GameObject chatBox;

    private Text text;


    private GameObject actualBox;

    bool moving;

    Vector3 moveDestination;
    Vector3 startPosition;
    float screenWidthUnit;
    float screenHeightUnit;


    private bool chatUp;

    private string[] script;
    public bool bugDone;



    private void initiateChatBox()
    {

        chatUp = true;


        Canvas c = GameObject.FindGameObjectWithTag("canvas").GetComponent<Canvas>();

        chatBox = Instantiate(textBox, c.transform);


        text = chatBox.GetComponentInChildren<Text>();

        chatBox.GetComponent<Canvas>().overrideSorting = true;


    }


    IEnumerator sayLine(string line)
    {


        string[] wordList = line.Split(' ');


        string textToShow = "";
        string prevword = "";
        int charCount = -2;

        float timer;
        float maxWait = 1f;

        if (!chatUp)
        {
            initiateChatBox();
        }

        foreach (string w in wordList)
        {


           
            textToShow += prevword + w + "  ";

            text.text = textToShow;

   

                float waittime = 0.05f;

                yield return new WaitForSeconds(waittime);




            if (text.cachedTextGenerator.characterCount < textToShow.Length)
            {
                
                prevword = w + "  ";
                text.text = textToShow;

                bool wait = true;

                timer = 0;

                while (wait)
                {

                    timer += Time.deltaTime;
                    if (Input.GetMouseButtonDown(0))
                    {
                        wait = false;
                        textToShow = "";

                        yield return new WaitForSeconds(0);
                    }
                    yield return null;


                }


            }
            else prevword = "";






        }


        timer = 0;

        bool wait2 = true;
        while (wait2)
        {

            timer += Time.deltaTime;
            if (Input.GetMouseButtonDown(0))
            {
                wait2 = false;
                yield return new WaitForSeconds(0);
            }


            yield return null;
        }






    }


    IEnumerator enterScreen()
    {

        moving = true;
        moveDestination = new Vector3(transform.position.x, -(screenHeightUnit * 0.5f) * 0.3f, 1);
        while (moving)
        {

            if (transform.position == moveDestination)
            {
                moving = false;

                yield return new WaitForSeconds(0f);
            }

            //initiateChatBox ();


            yield return null;


        }


    }

    IEnumerator leaveScreen()
    {


        if (chatUp)
        {


            Destroy(chatBox);
            chatUp = false;

        }



        moving = true;
        moveDestination = startPosition;
        while (moving)
        {

            if (transform.position == moveDestination)
            {
                moving = false;

                yield return new WaitForSeconds(0f);
            }


            yield return null;


        }


    }





    IEnumerator handleScript(int speedFactor)
    {


        foreach (string scriptLine in script)
        {



            string command = scriptLine.Split('^')[0].Trim();


            switch (command)
            {

                case "enter":
                    yield return StartCoroutine(enterScreen());
                    break;

                case "say":
                    yield return StartCoroutine(sayLine(scriptLine.Split('^')[1]));
                    break;

                case "equip":
                    equippedItem.sprite = itemsToEquip[Convert.ToInt32(scriptLine.Split('^')[1])];
                    break;
                case "unequip":
                    equippedItem.sprite = null;
                    break;
                case "finito":
                    LevelManager.Instance.blockClicks = false;
                    Destroy(gameObject);
                    break;

                case "leave":
                    yield return StartCoroutine(leaveScreen());
                    break;
                case "runbug":
                    yield return StartCoroutine(runBug());
                    break;
                case "present":
                    yield return presentObject(scriptLine.Split('^')[1]);
                    break;
                case "stoppresent":
                    stopPresent();
                    break;

                case "load":
                    LevelManager.Instance.PickLevel("tutorial" + scriptLine.Split('^')[1]);
                    SceneManager.LoadScene("level");
                    if (chatUp)
                    {
                        Destroy(chatBox);
                        chatUp = false;
                        yield return new WaitForSeconds(0.25f);
                    }
                    break;



            }


        }




    }

    private IEnumerator runBug()
    {
        GameObject.FindGameObjectWithTag("runbug").GetComponent<playLevel>().BritStartRunner();

        yield return new WaitUntil(() => bugDone);

    }

    private void stopPresent()
    {
        Destroy(actualBox);
    }

    IEnumerator presentObject(string v)
    {

        char[] charList = v.ToCharArray();
        Canvas c = GameObject.FindGameObjectWithTag("canvas").GetComponent<Canvas>();

        actualBox = Instantiate(presentBox, c.transform);

        SpriteRenderer presentIcon = ShroomList[charList[0]].GetComponentInChildren<SpriteRenderer>();

        float widthRatio = (presentIcon.bounds.size.x / presentIcon.bounds.size.y);

        Image icon = actualBox.GetComponentsInChildren<Image>()[1];

        icon.sprite = presentIcon.sprite;
        icon.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.height * 0.35f * widthRatio, Screen.height * 0.35f);


        yield return null;
    }



    private void loadScript(string scriptName)
    {

        Debug.Log("acts/" + LevelManager.Instance.pickedAct.idCode + "/britScripts/" + scriptName);

        TextAsset bindData = Resources.Load("acts/" + LevelManager.Instance.pickedAct.idCode + "/britScripts/" + scriptName) as TextAsset;

        string data = bindData.text.Replace(Environment.NewLine, string.Empty);

        script = data.Split('-');



    }

    public void initiateBrit()
    {


        LevelManager.Instance.blockClicks = true;
        StartCoroutine(handleScript(1));


    }


    private void move()
    {

        transform.position = Vector3.MoveTowards(transform.position, moveDestination, 5 * Time.deltaTime);



    }


    // Use this for initialization
    void Awake()
    {



        //gameObject.transform.position = new Vector3 (8,0,0);

        float britHeight = ((float)Camera.main.orthographicSize * 2.0f);

        screenHeightUnit = (float)Camera.main.orthographicSize;
        screenWidthUnit = screenHeightUnit * Camera.main.aspect;

        gameObject.transform.position = new Vector3(-(screenWidthUnit * 0.7f), -(screenHeightUnit * 2f), 1);

        gameObject.transform.localScale = new Vector3(britHeight, britHeight, 5);

        SpriteRenderer sprite = GetComponent<SpriteRenderer>();

        startPosition = transform.position;

        ShroomList = dictionary.getDictionary();


        DontDestroyOnLoad(gameObject);


    }

    public void playScript(string scriptToPlay)
    {

        loadScript(scriptToPlay);
        initiateBrit();

    }

    // Update is called once per frame
    void Update()
    {

        if (moving)
        {

            move();

        }


    }
}
