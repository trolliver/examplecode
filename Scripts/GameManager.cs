﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager> {


	[SerializeField]
	private GameObject startBtn;

	[SerializeField]
	private GameObject stopBtn;

	public bool running {get; private set;}

	public bool waiting {get; private set;}

	public InventoryTile ChosenInv { get; private set; }

	public string chosenLevel { get; private set; }

    Color clr;

    
	public void startGame(){

		running = true;
		waiting = true;
		startBtn.SetActive(false);
	}

	public void runnerDone(){

		running = false;
		stopBtn.SetActive(false);
		//startBtn.SetActive (true);
	}

	public void waitingDone(){

		waiting = false;
		stopBtn.SetActive (true);
		startBtn.SetActive (true);
	}

	public void triggerStop(){
	
		runnerDone ();
		waitingDone ();
	
	}



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PickLevel(string level){

		this.chosenLevel = level;

	}


	public void PickTower(InventoryTile invTile)
	{

		if (this.ChosenInv != null) {

			Image previousChosenTile = this.ChosenInv.gameObject.GetComponent<Image> ();
            previousChosenTile.color = clr;
            
            
           
			previousChosenTile.GetComponentInChildren<Text> ().color = Color.white;

		}



		Image chosenTile = invTile.gameObject.GetComponent<Image>();
        clr = chosenTile.color;
        chosenTile.color = new Color(0,0.64f,0);
        //chosenTile.color = Color.blue;

        chosenTile.GetComponentInChildren<Text> ().color = Color.yellow;


		this.ChosenInv = invTile;

	}

}
