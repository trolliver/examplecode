﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Point
{

	public int X { get; set;}
	public int Y { get; set;}

	public Point(int x, int y):this()
	{
		this.X = x;
		this.Y = y;
	}

	public override string ToString ()
	{
		return string.Format ("[Point: X={0}, Y={1}]", X, Y);
	}


	public override bool Equals(object obj){

		if(!(obj is Point))
			return false;

		Point p = (Point)obj;

		return p.X == this.X && p.Y == this.Y;


	}



    public static bool operator ==(Point one, Point two)
    {


        return one.X == two.X && one.Y == two.Y;


    }



    public static bool operator !=(Point one, Point two)
    {


        return one.X != two.X || one.Y != two.Y;


    }

	public static Point operator -(Point x, Point y){
		return new Point (x.X - y.X, x.Y - y.Y);
	}


}
