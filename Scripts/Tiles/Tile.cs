﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

	public Point GridPos { get; set;}

    public Vector3 worldPos { get; set; }

    public bool isEmpty { get; private set; }

    public bool isChangeable { get; private set; }

    public bool isPassable { get; private set; }

    private GameObject shroom;

	private char shroomKey;

    private InventoryTile placer;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Setup(Point gridPos, Vector3 worldPos, Vector3 size)
	{
		shroomKey = 'O';
		this.GridPos = gridPos;
        this.worldPos = worldPos;
		this.transform.position = worldPos;
		transform.localScale = size;
		loadLevel.Instance.Tiles.Add (gridPos, this);
        isEmpty = true;
        isPassable = true;
        isChangeable = true;

    }




	public void assignDefaultShroom(GameObject shroomType, char key)
	{

        if (shroomType.GetComponent<CapsulatedShroom>() != null)
        {
            shroom = Instantiate(shroomType, transform.position, Quaternion.identity, transform);
            shroom.transform.localPosition = new Vector3(0.5f, shroomType.GetComponent<CapsulatedShroom>().yValue, 1);
            shroom.transform.localScale = new Vector3(1, 1, 1);
            shroomType = shroom.GetComponent<CapsulatedShroom>().shroom;
            shroom = shroomType;
            setDepth();

        }
        else {
            shroom = Instantiate(shroomType, transform.position, Quaternion.identity, transform);
            shroom.transform.position += new Vector3(0, 0, 1);
            shroom.transform.localScale = new Vector3(1, 1, 1);
            setDepth();

        }


        isEmpty = false;
        isChangeable = false;

		this.shroomKey = key;

        if (shroomType.GetComponent<shroomInfo>().groundTile)
        {

            shroom.GetComponent<SpriteRenderer>().sortingOrder = (GridPos.Y + 1) * 10 - 20;

        }





        isPassable = shroomType.GetComponent<shroomInfo>().passable;
	}



	private void setDepth(){
	
		shroom.GetComponentInChildren<SpriteRenderer>().sortingOrder = (GridPos.Y+1)*10 + (10-GridPos.X);
       

    }


	private void OnMouseOver(){




	}

	public char ShroomKey {
		get {
			return shroomKey;
		}
	}


	public void setPassable(bool value){
	
		isPassable = value;
	
	}





    void OnMouseDown()
	{

		if (!GameManager.Instance.waiting && !LevelManager.Instance.blockClicks) {

			if (isEmpty && GameManager.Instance.ChosenInv.shroomsLeft ()) {

				isPassable = GameManager.Instance.ChosenInv.Shroom.GetComponentInChildren<shroomInfo> ().passable;

                if (GameManager.Instance.ChosenInv.Shroom.GetComponent<CapsulatedShroom>() != null)
                {
                    shroom = Instantiate(GameManager.Instance.ChosenInv.Shroom, transform.position, Quaternion.identity, transform);
                    shroom.transform.localPosition = new Vector3(0.5f, GameManager.Instance.ChosenInv.Shroom.GetComponent<CapsulatedShroom>().yValue, 1);
                    shroom.transform.localScale = new Vector3(1, 1, 1);
                }
                else {

                    shroom = Instantiate(GameManager.Instance.ChosenInv.Shroom, transform.position, Quaternion.identity, transform);
                    shroom.transform.position += new Vector3(0, 0, 1);
                    shroom.transform.localScale = new Vector3(1, 1, 1);
                }


				setDepth ();

				


				isEmpty = false;
				isChangeable = false;



			

				if (PathFinder.GetPath ()) {

					GameManager.Instance.ChosenInv.place ();
					this.placer = GameManager.Instance.ChosenInv;
					this.shroomKey = GameManager.Instance.ChosenInv.Key;
					loadLevel.Instance.addToPlaced (GridPos);
					isChangeable = true;


				} else {


					//shroom.gameObject.GetComponent<ShroomSpecific> ().removeShroom();
					StartCoroutine (WrongPlacedShroom(125f/1000f));
					isPassable = true;





				}
			} else if (isChangeable && !isEmpty) {

                if (testPickUpShroom())
                {
                    pickUpShroom();
                    loadLevel.Instance.pickUpFromPlaced(GridPos);
                }
                else {
                    Debug.Log("not allowed");
                }
				
              
                
                

				

			
			}
			else if(LevelManager.Instance.editMode && !isEmpty){

				if(shroom.GetComponent<ShroomSpecific>() != null){

					shroom.GetComponent<ShroomSpecific> ().removeShroom ();


				}


				Destroy (shroom);
				this.shroomKey = 'O';
				isEmpty = true;
				isPassable = true;

			}



		}
	}


	IEnumerator WrongPlacedShroom(float timeInSeconds)
	{
		SpriteRenderer chosenTile = shroom.GetComponentInChildren<SpriteRenderer> ();
		chosenTile.color = Color.red;

		if(shroom.GetComponentInChildren<ShroomSpecific>() != null){

			shroom.GetComponentInChildren<ShroomSpecific> ().removeShroom ();


		}

			



	
		yield return new WaitForSeconds (timeInSeconds);

		Color tmp = chosenTile.color;
		tmp.a = 0.75f;
		chosenTile.color = tmp;

		yield return new WaitForSeconds (timeInSeconds);

		tmp = chosenTile.color;
		tmp.a = 0.5f;
		chosenTile.color = tmp;


		yield return new WaitForSeconds (timeInSeconds);

		tmp = chosenTile.color;
		tmp.a = 0.25f;
		chosenTile.color = tmp;


		yield return new WaitForSeconds (timeInSeconds);
		Destroy (shroom);
		isEmpty = true;
		isChangeable = true;
	}



	public void pickUpShroom(){
	
		if(shroom.GetComponentInChildren<ShroomSpecific>() != null){

			shroom.GetComponentInChildren<ShroomSpecific> ().removeShroom ();

		}


		Destroy (shroom);
		isEmpty = true;
		placer.pickUp ();
		this.shroomKey = 'O';
		isPassable = true;
	
	
	}

    public bool testPickUpShroom() {

        char tempKey = this.shroomKey;
        bool tempPass = isPassable;
        if (shroom.GetComponentInChildren<ShroomSpecific>() != null)
        {

            shroom.GetComponentInChildren<ShroomSpecific>().removeShroom();

        }
        this.shroomKey = 'O';
        isEmpty = true;
        isPassable = true;

        bool worked = PathFinder.GetPath();

        if (!worked) {
            isEmpty = false;
            isPassable = tempPass;
            this.shroomKey = tempKey;
            loadLevel.Instance.addBait(GridPos);
            

        }

        return worked;
    }

    }






