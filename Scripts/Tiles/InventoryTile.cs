﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryTile : MonoBehaviour {


	Text newText;


    [SerializeField]
	private GameObject shroom;

	public GameObject Shroom {
		get {
			return shroom;
		}
	
	}

	char key;

	int count;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void setEmpty(){


		this.shroom = null;
	
		gameObject.GetComponentsInChildren<Image> () [1].enabled = false;


	}

	public char Key {
		get {
			return key;
		}
	}



	public void AssignShroom(GameObject shroom, int count, char key, int sizeCount){


        if (shroom.GetComponent<CapsulatedShroom>() != null)
        {
            //shroom = shroom.GetComponent<CapsulatedShroom>().shroom;
        }

		
		this.shroom = shroom;
		this.count = count;
		this.key = key;


		gameObject.GetComponent<Button> ().onClick.AddListener (() => setShroom ());

		float widthRatio = (shroom.GetComponentInChildren<SpriteRenderer> ().bounds.size.x/shroom.GetComponentInChildren<SpriteRenderer> ().bounds.size.y);


		GetComponentsInChildren<RectTransform> () [1].sizeDelta = new Vector2 (widthRatio*Screen.height*Camera.main.rect.height*0.9f/sizeCount, Screen.height*Camera.main.rect.height*0.9f/sizeCount);
		
       
		Vector3 ikonPos = new Vector3(transform.position.x + (transform.localScale.x / 3)-(transform.localScale.x / 4), transform.position.y - transform.localScale.y / 4, 1);

		//Vector3 ikonPos = new Vector3(transform.localScale.x / 2 - 1/2,- transform.localScale.y / 2 + 1/2, 1);

		gameObject.GetComponentsInChildren<Image> ()[1].sprite = shroom.GetComponentInChildren<SpriteRenderer> ().sprite;

		GameObject shroomIcon = Instantiate (shroom, ikonPos, Quaternion.identity, transform);

		shroomIcon.transform.localScale = new Vector3(0.5f, 0.5f, 1);

		shroomIcon.GetComponentInChildren<SpriteRenderer>().sortingOrder = 212;
        //shroomIcon.transform.localScale = this.transform.localScale * new Vector3(1, 2, 1);

        //shroomIcon.transform.localScale = new Vector3(1.33f, 1.33f, 1);

	

		newText = GetComponentInChildren<Text> ();
		newText.text = "x" + count;
		//GetComponentInChildren<MeshRenderer> ().sortingOrder = 213;


		if (count > 99) {

			//GetComponentInChildren<MeshRenderer> ().enabled = false;

		}


			

       

        //shroomIcon.transform.SetParent (transform);
	
	}

    public void place()
    {

        

        
            this.count--;
            newText.text = "x" + count;

    }

    public bool shroomsLeft()
    {
        return this.count > 0;
    }

    public void pickUp()
    {
           
            this.count++;
            newText.text = "x" + count;

            
        
        
    }


    public void Setup(Vector3 worldPos, Vector3 size)
	{
		this.transform.position = worldPos;
		transform.localScale = size;
		//GetComponent<SpriteRenderer>().sortingOrder = 210;

	}
		
	void setShroom(){

		Debug.Log (count);


		if (this.shroom != null) {
		
			GameManager.Instance.PickTower (this);

		}



	}




}
