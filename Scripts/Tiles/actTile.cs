﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class actTile : MonoBehaviour {

	public string title;

	public string idCode;

    public string shortPresent;

	public Sprite menuImage;

	private string lockReason;

    public bool buyAble;

	//Only for giving user feedback.
	[SerializeField]
	private string preReqActTitle;

    public bool comingSoon;

    //Information about requirements to unlock

    public bool[] tutLevels;

    
    

    [SerializeField]
	bool startingAct;
	[SerializeField]
	private string preReqActCode;

	[SerializeField]
	private int preReqActStars;

	[SerializeField]
	private int totalStarsReq;
	[SerializeField]
	bool buyReq;


    public bool checkLock(){


        if (comingSoon) {
            lockReason = "Will be released soon...";
            return false;
        }

        if (startingAct) {
			unlockAct ();
			return true;
		}
		if (buyReq && !SaveLoad.save.checkIfBoughtAct (idCode)) {
			lockReason = "This Act can only be unlocked by purchasing it.";
			return false;
		}

		//if (!SaveLoad.save.getAct (preReqActCode).Cleared) {
			
			//lockReason = "You need to clear the chapter: " + preReqActTitle + " first";
			//return false;
		//}

		int stars = SaveLoad.save.getAct (preReqActCode).updateStars();
		if (!(stars >= preReqActStars) && !SaveLoad.save.checkIfBoughtAct(idCode)) {

            lockReason = "Need " + (preReqActStars - stars)  + " more danger points in: '" + preReqActTitle + "' to unlock this Act or purchase to unlock now ";
           
			//lockReason = "You need to have a hazardous level of at least: " + preReqActStars.ToString() +  " in the act: '" + preReqActTitle + "' , missing: " + (preReqActStars - stars) + " hazard points";
			return false;
		}
				
		

		return true;
	
	
	}

    public string getLockReason() {

    return lockReason;
    }

	private void unlockAct(){
	
		SaveLoad.save.getAct (idCode).Unlocked = true;
	
	}



}
