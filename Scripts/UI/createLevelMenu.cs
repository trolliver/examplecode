﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class createLevelMenu : MonoBehaviour {

    public Color goldColor;
    public Color silverColor;
    public Color bronzeColor;
    public Color unplayedColor;
    public Color lockedColor;

    public Sprite goldMedal;

    [SerializeField]
	private GameObject backBtn;

	[SerializeField]
	private GameObject levelButton;

	[SerializeField]
	private GameObject tutorialButton;

	[SerializeField]
	private GameObject levelMenuCanvas;

	[SerializeField]
	private GameObject theBrit;

	// Use this for initialization
	 void Start () {

	

		Vector3 worldStart = Camera.main.ScreenToWorldPoint (new Vector3 (0, Screen.height));


		float width = (float)(Camera.main.orthographicSize * 2.0 * Screen.width / Screen.height)/20;

		//Color[] buttonColorList = new Color[5]{new Color32(111, 111, 111, 255), new Color32(118, 152, 102, 255), new Color32(73, 92, 0, 255), new Color32(27, 79, 24, 255), new Color32(101, 19, 19, 255)};
		Color[] buttonColorList = new Color[5]{unplayedColor, bronzeColor, silverColor, goldColor, lockedColor};
		//Color[] buttonColorList = new Color[5]{new Color32(111, 111, 111, 255), new Color32(118, 152, 102, 255), new Color32(73, 92, 0, 255), new Color32(37, 156, 0, 255), new Color32(101, 19, 19, 255)};

		Act levelProgress = SaveLoad.save.getAct (LevelManager.Instance.pickedAct.idCode);

		GameObject backButton = Instantiate (backBtn, levelMenuCanvas.transform);
		backButton.GetComponentInParent<Button>().onClick.AddListener (() => backToActs());
        int currentStars = levelProgress.updateStars();

        int playBrit = 10;
	

		for (int row = 0; row < 4; row++) {

            RectTransform tutSize = Instantiate(tutorialButton).GetComponent<RectTransform>();
            Button tutBtn = tutSize.GetComponent<Button>();
            tutSize.transform.SetParent(levelMenuCanvas.transform, false);
            tutSize.anchorMin = new Vector2(0.08f, 1 - (0.1f + (row + 1) * 0.2f));
            tutSize.anchorMax = new Vector2(0.08f + 1 * 0.12f, 1 - (0.1f + (row) * 0.2f));

            bool hasTut = LevelManager.Instance.pickedAct.tutLevels[row];


            

            if (levelProgress.checkLevelLock(row * 5))
            {
                

                Sprite img = tutSize.GetComponent<tutorialButton>().unlockedImage;
                tutSize.GetComponent<Image>().sprite = img;
                tutBtn.GetComponentInChildren<Text>().text = "";
                //string scriptName = "tutorialScript" + (row + 1).ToString();
                if (hasTut)
                {
                    string scriptName = "tutorialScript" + (row + 1).ToString();


                    tutBtn.onClick.AddListener(() => summonBrit(scriptName));
                    //tutBtn.GetComponentInChildren<Text>().text = "";

                    if (!levelProgress.hasPlayedTutLevel((row+1)))
                    {
                       
                        playBrit = row;
                    }
                }
                

            }
            else {
                tutBtn.GetComponentInChildren<Text>().text = (row*10 - currentStars).ToString();
            }
            if (!hasTut) tutSize.GetComponent<Image>().color = new Color(0,0,0,0);

            for (int i = 0; i < 5; i++) {

				RectTransform test = Instantiate (levelButton).GetComponent<RectTransform>();

				//test.transform.parent = levelMenuCanvas.transform;
				test.transform.SetParent(levelMenuCanvas.transform, false);
				float size = Screen.width / 7;

				test.anchorMin = new Vector2 (0.05f + (i+1)*0.15f, 1 - (0.1f + (row+1)*0.2f));
				test.anchorMax = new Vector2 (0.05f + (i+2)*0.15f, 1 - (0.1f + (row)*0.2f));
				//test.position = new Vector3 (0, 0 , 1);

				//test.sizeDelta = new Vector2 (size, size/2);
				test.transform.localScale = new Vector3 (1, 1, 1);

				int indexNumber = i + 5 * row;
				test.GetComponentInChildren<Text> ().text = (i + 1 + 5*row).ToString ();
				//test.transform.localPosition = new Vector3 (0 ,0 , 1);

				//Set color based on levelprogress


				Button levelBtn = test.GetComponent<Button> ();
			 	Image chosenTile = levelBtn.GetComponentInParent<Image> ();



				if (LevelManager.Instance.editMode) {

                    
                    chosenTile.color = buttonColorList [0];

						//chosenTile.color = buttonColorList [levelProgress.checkLevelProgress (indexNumber)];
						levelBtn.onClick.AddListener (() => setLevel (test.GetComponentInChildren<Text> ().text));




				} else {
					

					if (!levelProgress.checkLevelLock (indexNumber)) {

						chosenTile.color = buttonColorList [4];
                        levelBtn.onClick.AddListener(() => summonBrit("progress"));


                    } else {
                        int progress = levelProgress.checkLevelProgress(indexNumber);


                        chosenTile.color = buttonColorList [progress];
                        if (progress.Equals(3)){
                            test.GetComponent<Image>().sprite = goldMedal;
                        }
						levelBtn.onClick.AddListener (() => setLevel (test.GetComponentInChildren<Text> ().text));

					}
				}




			}



		}

		if(playBrit < 10){

			//string storyToPlay = "tutorialScriptStory" + (playBrit + 1).ToString ();
			string storyToPlay = "tutorialScript" + (playBrit + 1).ToString ();
			Debug.Log ("before");
           
            SaveLoad.save.getAct(LevelManager.Instance.pickedAct.idCode).playTutLevel(playBrit+1);
           
			SaveLoad.Save ();
			Debug.Log ("after: " +levelProgress.hasPlayedTutLevel (playBrit) + "  the brit Played at row index: " + playBrit);
			summonBrit (storyToPlay);


		}



	
	}

	void backToActs ()
	{
		if (!LevelManager.Instance.blockClicks) {
			SceneManager.LoadScene ("actMenu");
		}
	}

	private void setLevel(string level){
	
		if (!LevelManager.Instance.blockClicks) {
		
			LevelManager.Instance.PickLevel (level);

			SceneManager.LoadScene ("level");
		
		}





	
	}


	private void summonBrit(string tutLevel){

		if (!LevelManager.Instance.blockClicks) {

			Brit brit = Instantiate (theBrit).GetComponent<Brit> ();
			brit.playScript (tutLevel);
		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
