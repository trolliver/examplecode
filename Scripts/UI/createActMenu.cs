﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Purchasing;

public class createActMenu : MonoBehaviour {

	[SerializeField]
	private GameObject titlePanel;

    [SerializeField]
    private GameObject buyBtn;

    [SerializeField]
    private GameObject lockedMessage;

    [SerializeField]
    private GameObject currentScore;
    [SerializeField]
	private GameObject imagePanel;

    [SerializeField]
    private GameObject framePanel;

    [SerializeField]
	private GameObject canvas;

	[SerializeField]
	private GameObject chooseBtn;

	[SerializeField]
	private GameObject leftArrow;

	[SerializeField]
	private GameObject rightArrow;

	[SerializeField]
	private actTile[] actList;

	[SerializeField]
	private Sprite lockedActImage;

    [SerializeField]
    private Sprite comingSoon;

    [SerializeField]
	private GameObject backToMainMenuBtn;

	[SerializeField]
	private Shader shader;


	private Material defaultMaterial;

	private int counter = 0;


	private int currentAct(){
		return (counter % actList.Length + actList.Length) % actList.Length;
	}


	private GameObject title;
	private GameObject image;
    private GameObject lockedText;
    private GameObject scoreText;
    

    private bool currentActUnlocked;

	void Awake(){
		float titleWidth = Screen.width / 3;
		float titleHeight = Screen.height / 10;
		float verticalSpacing = Screen.width / 20;


		

		GameObject backBtn = Instantiate (backToMainMenuBtn, canvas.transform);
		backBtn.GetComponent<RectTransform>().anchorMin = new Vector2(0.795f, .005f);
		backBtn.GetComponent<RectTransform>().anchorMax = new Vector2(0.995f, 0.2f);
		backBtn.transform.localScale = new Vector3 (1, 1, 1);
		backBtn.GetComponentInChildren<Text> ().text = "BACK";
		backBtn.GetComponentInParent<Button> ().onClick.AddListener (() =>  backToMainMenu());


		title = Instantiate (titlePanel, canvas.transform);
		//title.GetComponent<RectTransform>().sizeDelta = new Vector2(titleWidth, titleHeight);
		title.GetComponent<RectTransform>().anchorMin = new Vector2(0.27f, 0.83f);
		title.GetComponent<RectTransform>().anchorMax = new Vector2(0.73f, 0.97f);
		title.transform.localScale = new Vector3 (1, 1, 1);
        //title.transform.localPosition = new Vector3 (0, titleWidth/4 + Screen.height/4, 1);

        

        image = Instantiate (imagePanel, canvas.transform, true);
		image.GetComponent<RectTransform>().sizeDelta = new Vector2(titleWidth, titleWidth);
		image.transform.localScale = new Vector3 (1, 1, 1);
		image.transform.localPosition = new Vector3 (0, 0, 1);

        GameObject frame = Instantiate(framePanel, canvas.transform);
        frame.GetComponent<RectTransform>().sizeDelta = new Vector2(titleWidth, titleWidth);
        frame.transform.localScale = new Vector3(1, 1, 1);
        frame.transform.localPosition = new Vector3(0, 0, 1);

        lockedText = Instantiate(lockedMessage, canvas.transform);
        lockedText.GetComponent<RectTransform>().sizeDelta = new Vector2(titleWidth, titleWidth/2f);
        lockedText.transform.localScale = new Vector3(1, 1, 1);
        lockedText.transform.localPosition = new Vector3(0, 0, 1);
        lockedText.GetComponentInChildren<Text>().fontSize = Mathf.RoundToInt(titleWidth / 8);

        scoreText = Instantiate(currentScore, canvas.transform);
        scoreText.GetComponent<RectTransform>().sizeDelta = new Vector2(titleWidth, titleWidth / 2f);
        scoreText.transform.localScale = new Vector3(1, 1, 1);
        scoreText.transform.localPosition = new Vector3(0, 0, 1);
        scoreText.GetComponent<Text>().fontSize = Mathf.RoundToInt(titleWidth / 8);
        scoreText.GetComponent<RectTransform>().anchoredPosition = (new Vector2(0,0));
       
        


        //defaultMaterial = image.GetComponentInChildren<Image>().material;


        GameObject playBtn = Instantiate (chooseBtn, canvas.transform);
		playBtn.transform.localScale = new Vector3 (1, 1, 1);
		playBtn.GetComponentInParent<Button> ().onClick.AddListener (() =>  selectAct());

	
		GameObject leftBtn = Instantiate (leftArrow, canvas.transform);
		//leftBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(titleHeight*2, titleWidth);
		leftBtn.GetComponent<RectTransform>().anchorMin = new Vector2(0.15f, 0.33f);
		leftBtn.GetComponent<RectTransform>().anchorMax = new Vector2(0.25f, 0.66f);
		leftBtn.transform.localScale = new Vector3 (1, 1, 1);
		//leftBtn.transform.localPosition = new Vector3 (-(titleWidth - titleWidth/4)/2- titleHeight, -(titleWidth/4 + Screen.height/4), 1);
		leftBtn.GetComponentInParent<Button> ().onClick.AddListener (() =>  moveLeft());

		GameObject rightBtn = Instantiate (rightArrow, canvas.transform);
		rightBtn.GetComponent<RectTransform>().anchorMin = new Vector2(0.75f, 0.33f);
		rightBtn.GetComponent<RectTransform>().anchorMax = new Vector2(0.85f, 0.66f);
		rightBtn.transform.localScale = new Vector3 (1, 1, 1);
		//rightBtn.transform.localPosition = new Vector3 (+(titleWidth - titleWidth/4)/2+ titleHeight, -(titleWidth/4 + Screen.height/4), 1);
		rightBtn.GetComponentInParent<Button> ().onClick.AddListener (() =>  moveRight());

        buyBtn.transform.SetAsLastSibling();
		

		updateAct ();
	

	}


	public void moveLeft(){

		counter--;
		updateAct ();


    }

	public void moveRight(){

		counter++;
		updateAct ();
        

    }


	public void updateAct(){
        buyBtn.gameObject.SetActive(false);
        lockedText.SetActive(false);
        //lockedText.GetComponentInChildren<Text>().text = "";
        scoreText.GetComponent<Text>().text = "";

        title.GetComponentInChildren<Text>().text = actList[currentAct()].title;


			//LevelManager.Instance.PickAct (actList [currentAct ()].idCode);
			LevelManager.Instance.chooseAct(actList[currentAct()]);

		currentActUnlocked = LevelManager.Instance.pickedAct.checkLock();

		image.GetComponentInChildren<Image> ().sprite = LevelManager.Instance.pickedAct.menuImage;

        //testing for unlocking

        if (LevelManager.Instance.pickedAct.comingSoon) {
            image.GetComponentInChildren<Image>().sprite = comingSoon;
            scoreText.GetComponent<Text>().text = "Coming soon";
        }
        else
        {

            if (currentActUnlocked)
            {
                
                image.GetComponent<Image>().sprite = actList[currentAct()].menuImage;
                scoreText.GetComponent<Text>().text = currentActScore() + "/60";
               

            }
            else
            {

                buyBtn.gameObject.SetActive(true);
                string idCode = LevelManager.Instance.pickedAct.idCode; ;
                buyBtn.GetComponent<IAPButton>().productId = idCode;
               


                image.GetComponentInChildren<Image>().sprite = lockedActImage;
            }
        }
	}
	public void selectAct(){

        if (currentActUnlocked)
        {
            SceneManager.LoadScene("levelMenu");
        }
        else {
            lockedText.SetActive(true);

            lockedText.GetComponentInChildren<Text>().text = actList[currentAct()].getLockReason();
        }

	
	}

    private string currentActScore() {
        return SaveLoad.save.getAct(LevelManager.Instance.pickedAct.idCode).updateStars().ToString();
       
    }


	public void backToMainMenu(){


			SceneManager.LoadScene ("mainMenu");
	

	}

}