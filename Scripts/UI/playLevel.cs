﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class playLevel : MonoBehaviour {




	[SerializeField]
	private GameObject btn;


    public GameObject bug;


	private Bug myra;

    [SerializeField]
    private GameObject theBrit;

    public void StartRunner()
    {
		if (!LevelManager.Instance.blockClicks) {
		
			GameManager.Instance.startGame ();
			bool solved = PathFinder.GetPath();

			myra = Instantiate(bug).GetComponent<Bug>();
			myra.Spawn();

			btn.SetActive (false);

			loadLevel.Instance.clearStopButtonSwitch ();

		
		}

	
        
    }

    public void BritStartRunner() {

        GameManager.Instance.startGame();
        bool solved = PathFinder.GetPath();
        myra = Instantiate(bug).GetComponent<Bug>();
        myra.Spawn();
        myra.britRunner = true;
        //myra.britToNotify = brit;
        btn.SetActive(false);

        loadLevel.Instance.clearStopButtonSwitch();
    }


	public void StopRunner()
	{
        if (!LevelManager.Instance.blockClicks)
        {
            myra.cancelEarly();
            loadLevel.Instance.clearStopButtonSwitch();
        }
	
	
	}


	private void setupSneezers (Bug bug){
		
		HashSet<Point> sneezers = loadLevel.Instance.getSneezers ();
			
	}

	public void retryLevel(){
	
	
		myra.retryLevel ();
		loadLevel.Instance.showFinishButtons (false, false);


	}

	public void nextLevel(){

		int levelToCheck;

		if (LevelManager.Instance.chosenLevel.Contains ("tutorial")) {


			int indexOfNumber = LevelManager.Instance.chosenLevel.Length-1;

			levelToCheck = (5*(int.Parse(LevelManager.Instance.chosenLevel[indexOfNumber].ToString())-1))+1;



		} else {
		
			levelToCheck = int.Parse (LevelManager.Instance.chosenLevel) + 1;


		}

        int level = levelToCheck - 1;
		if (SaveLoad.save.getAct (LevelManager.Instance.pickedAct.idCode).checkLevelLock (level)) {

            Debug.Log((level / 5) + 1);

            if (level % 5 == 0 && LevelManager.Instance.pickedAct.tutLevels[(level / 5)] && !SaveLoad.save.getAct(LevelManager.Instance.pickedAct.idCode).hasPlayedTutLevel((level/5)+1))
            {

                string scriptName = "tutorialScript" + ((level / 5) + 1).ToString();

                SaveLoad.save.getAct(LevelManager.Instance.pickedAct.idCode).playTutLevel((level / 5) + 1);
               
                SaveLoad.Save();
                summonBrit(scriptName);



            }
            else {

                LevelManager.Instance.PickLevel(levelToCheck.ToString());

                SceneManager.LoadScene("level");

            }


		}
	
	

	}

    private void summonBrit(string tutLevel)
    {

        if (!LevelManager.Instance.blockClicks)
        {

            Brit brit = Instantiate(theBrit).GetComponent<Brit>();
            brit.playScript(tutLevel);
        }

    }


}
