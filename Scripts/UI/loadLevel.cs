﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Text;

public class loadLevel : Singleton<loadLevel> {


    public Point startPos { get; set; }

    public Point finishPos { get; set; }

	public bool hasBait { get; private set; }
	public Point baitPos { get; private set; }

	public bool hasBait2 { get; private set; }
	public Point bait2Pos { get; private set; }

	private HashSet<Point> sneezers;

	private HashSet<Point> waiters;

	private HashSet<Point> poofers;


	private HashSet<Point> placedShroomsList;

	[SerializeField]
	private GameObject canvas;

	[SerializeField]
	private GameObject leftPaneMask;

	[SerializeField]
	private GameObject rightPaneMask;


	[SerializeField]
	private GameObject buildModeSwitchBtn;
	[SerializeField]
	private GameObject saveBtn;


	[SerializeField]
	private GameObject nextBtn;
	[SerializeField]
	private GameObject stayBtn;

    [SerializeField]
    private GameObject currentLevelTextPrefab;

    private GameObject currentLevelText;

    [SerializeField]
	private GameObject startBtn;
	[SerializeField]
	private GameObject backBtn;


	[SerializeField]
	private GameObject stopBtn;

	[SerializeField]
	private GameObject clearBtn;


	[SerializeField]
	private GameObject bronzeTile;

	[SerializeField]
	private GameObject silverTile;

	[SerializeField]
	private GameObject goldTile;

	[SerializeField]
	private GameObject currentScoreTile;


	[SerializeField]
	private GameObject ShroomDictionary;


	public GameObject tile;
	public GameObject tile2;

	[SerializeField]
	private GameObject[] invTiles;

	private GameObject[] buildTileList;

	public GameObject placerTile;

	Vector3 worldStart;

	int invSlots;
	public int rowCount { get; private set; }
	public int colCount { get; private set; }


    float centeringValue;


    public Dictionary<char, GameObject> ShroomList { get; private set;}

	private bool standingScreen;

	public void ActivateBait ()
	{
		if (hasBait) {
			Tiles [baitPos].GetComponentInChildren<Bait> ().Active = true;
		} else{
						
			ActivateBait2 ();
						}

	}

	public void deactivateBait ()
	{
		if (hasBait) {
			Tiles [baitPos].GetComponentInChildren<Bait> ().Active = false;
		}

	}

	public void ActivateBait2 ()
	{
		if (hasBait2){
			Tiles [bait2Pos].GetComponentInChildren<Bait> ().Active = true;
	}
	}

	public void deactivateBait2 ()
	{
		if (hasBait2) {
			Tiles [bait2Pos].GetComponentInChildren<Bait> ().Active = false;
		}
	}

	public void notifyBait(){
		if(hasBait){Tiles[baitPos].GetComponentInChildren<Bait>().dodge();}
	}

	public Dictionary<Point, Tile> Tiles { get; set;}

	private string[] scoringData;

	public void addPoofer (Point gridPos)
	{
		poofers.Add (gridPos);

	}

	public void removePoofer (Point gridPos)
	{
		poofers.Remove (gridPos);

	}

	public HashSet<Point> getPoofers ()
	{
		return poofers;
	}

	public void addWaiter (Point gridPos)
	{
		waiters.Add (gridPos);

	}

	public void removeWaiter (Point gridPos)
	{
		waiters.Remove (gridPos);

	}

	public HashSet<Point> getWaiters ()
	{
		return waiters;
	}

	public void addZneezer (Point gridPos)
	{
		sneezers.Add (gridPos);

	}

	public void removeZneezer (Point gridPos)
	{
		sneezers.Remove (gridPos);

	}



	public HashSet<Point> getSneezers(){
	
		return sneezers;
	}

	public void addBait(Point position){

		baitPos = position;
		hasBait = true;
	
	
	}

	public void removeBait(){
	
		hasBait = false;
	
	}


	public void addBait2(Point position){

		bait2Pos = position;
		hasBait2 = true;


	}


	public void addToPlaced(Point position){

		placedShroomsList.Add (position);

	}

	public void pickUpFromPlaced(Point position){

		placedShroomsList.Remove (position);

	}

	public void clearPlaced(){
		if(!LevelManager.Instance.blockClicks){

			foreach (Point pos in placedShroomsList) {
				Tiles [pos].pickUpShroom ();
			}

			placedShroomsList.Clear ();
		}


	}



	public void removeBait2(){

		hasBait2 = false;

	}


	public void setStart(Point position){
	
		startPos = position;
	
	}

	public void setFinish(Point position){

		finishPos = position;
	}



	public float TileSize
	{
		
		get { return tile.GetComponent<SpriteRenderer> ().sprite.bounds.size.y; }
	}




	public void updateCurrentScore (double score){

		double rounded = Math.Round (score, 3);

	
		currentScoreTile.GetComponentInChildren<Text>().text = rounded.ToString();
	
	}


	public void clearStopButtonSwitch(){
	
		stopBtn.SetActive (!stopBtn.activeInHierarchy);
		clearBtn.SetActive (!clearBtn.activeInHierarchy);
	
	}




	// Use this for initialization
	void Awake () {




		ShroomList = ShroomDictionary.GetComponent<GameDictionary>().getDictionary();

		sneezers = new HashSet<Point> ();
		poofers = new HashSet<Point> ();
		placedShroomsList = new HashSet<Point> ();

		showFinishButtons (false, false);


		string[] mapData = ReadLevelText(LevelManager.Instance.pickedAct.idCode, "levelStructures", LevelManager.Instance.chosenLevel);

		rowCount = mapData.Length;

		colCount = mapData[0].Length;


		setupLevelReqs ();

		stopBtn.SetActive (!stopBtn.activeInHierarchy);

        

		worldStart = Camera.main.ScreenToWorldPoint (new Vector3 (0 + Screen.width*Camera.main.rect.x, Screen.height*(1.0f-Camera.main.rect.y)));


		float gridCellSize = (float) ((Camera.main.orthographicSize * 2.0)/rowCount);


		float endOfGridPos = (gridCellSize * colCount)/2;

		centeringValue = (float)((Camera.main.orthographicSize * 2.0 * Screen.width*Camera.main.rect.width / (Screen.height*Camera.main.rect.height)) - gridCellSize * colCount) / 2;

	
		float camHeight = Camera.main.rect.height;
		float camWidth = Camera.main.rect.width;


		float buttonsWidth = (float)1.0f-(((1.0f/rowCount)*(float)Screen.height*camHeight/((float)Screen.width*camWidth))*colCount);
		//float buttonsWidth = (float)1.0f-(((1.0f/rowCount)*(float)Screen.height*Camera.main.rect.height/((float)Screen.width*Camera.main.rect.width))*colCount);
		float buttonsHeight = (float)(1.0f/rowCount)*2;

		float signsHeight = (float)(1.0f - buttonsHeight*2)/4;

		buttonsHeight *= 0.67f;

		if (LevelManager.Instance.editMode) {
			buttonsHeight *= 0.75f;
			buttonsHeight *= 0.8f;

			saveBtn.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, buttonsHeight*4);
			saveBtn.GetComponent<RectTransform>().anchorMax = new Vector2(1, buttonsHeight*5);

			buildModeSwitchBtn.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, buttonsHeight*3);
			buildModeSwitchBtn.GetComponent<RectTransform>().anchorMax = new Vector2(1, buttonsHeight*4);

		}

		startBtn.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, 0);
		startBtn.GetComponent<RectTransform>().anchorMax = new Vector2(1, buttonsHeight);

		clearBtn.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, buttonsHeight);
		clearBtn.GetComponent<RectTransform>().anchorMax = new Vector2(1, buttonsHeight*2);

		stopBtn.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, buttonsHeight);
		stopBtn.GetComponent<RectTransform>().anchorMax = new Vector2(1, buttonsHeight*2);

		backBtn.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, buttonsHeight*2);
		backBtn.GetComponent<RectTransform>().anchorMax = new Vector2(1, buttonsHeight*3);
		backBtn.GetComponentInParent<Button> ().onClick.AddListener (() =>  backToMainMenu());


		goldTile.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, 1.0f-signsHeight);
		goldTile.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);

		silverTile.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, 1.0f-signsHeight*2);
		silverTile.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1 - signsHeight);

		bronzeTile.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, 1.0f-signsHeight*3);
		bronzeTile.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1 - signsHeight*2);

		currentScoreTile.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, 1.0f-signsHeight*4);
		currentScoreTile.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1 - signsHeight*3);

        


		int levelNbr;


		if (Int32.TryParse (LevelManager.Instance.chosenLevel, out levelNbr)) {
		
			updateCurrentScore (SaveLoad.save.getAct (LevelManager.Instance.pickedAct.idCode).checkBest (levelNbr - 1));
		} else {
			updateCurrentScore (0);
		}







		rightPaneMask.GetComponent<RectTransform>().anchorMin = new Vector2(1.0f-buttonsWidth/2, 0);
		rightPaneMask.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);

		rightPaneMask.GetComponent<Image> ().color = GetComponent<Camera> ().backgroundColor;

		leftPaneMask.GetComponent<RectTransform>().anchorMax = new Vector2(buttonsWidth/2, 1);

		leftPaneMask.GetComponent<Image> ().color = GetComponent<Camera> ().backgroundColor;






		Tiles = new Dictionary<Point, Tile> ();



		for(int y = 0; y<rowCount; y++)
		{
			string line = mapData [y].Trim();

			char[] newTiles = line.ToCharArray ();


			for(int x = 0; x<newTiles.Length; x++)
			{

				PlaceTile (x, y, worldStart, newTiles[x]);

					

			}
		}

		setupInventoryTiles ();


		if (LevelManager.Instance.editMode) {
			setupBuildTiles ();
		}



		
	}

	public void hideShowBuildTiles(){
	

		if (buildTileList [0].activeInHierarchy) {
		
			foreach (GameObject buildTile in buildTileList) {

				buildTile.SetActive (false);
			
			}


		
		} else {
		
			foreach (GameObject buildTile in buildTileList) {

				buildTile.SetActive (true);

			}
		
		
		}


	
	}


	private void PlaceInventoryTile(int x, int y, Vector3 worldStart, string inventoryInfo){
			
			//InventoryTile newTile;
			//newTile = Instantiate(placerTile).GetComponent<InventoryTile>();

		InventoryTile newTile =	invTiles[x].GetComponent<InventoryTile>();

			
		RectTransform rectTransform = newTile.GetComponent<RectTransform> ();

		float xValPercent = (float)1.0f-(((1.0f/rowCount)*(float)Screen.height*Camera.main.rect.height/(float)Screen.width*Camera.main.rect.width)*colCount);
		rectTransform.anchorMin = new Vector2(0, rectTransform.anchorMin.y);
		rectTransform.anchorMax = new Vector2(xValPercent/2, rectTransform.anchorMax.y);

			if (inventoryInfo != null) {

				inventoryInfo.Trim ();

				int count = int.Parse(inventoryInfo.Substring (1));
				char type = (char)inventoryInfo[0];

				newTile.AssignShroom (ShroomList [type], count, type, 4);

				if (x == 0) {

					GameManager.Instance.PickTower (newTile);
				}
					

            } else {
				newTile.setEmpty();
			}


		//newTile.transform.localScale = new Vector3 (width, width, 1.0f);
		//newTile.transform.position = new Vector3 (worldStart.x + (width * x), worldStart.y - (width * y), 1);

		if (LevelManager.Instance.editMode) {
		
			InputField field = newTile.GetComponentInChildren<InputField> ();
			field.interactable = true;
			field.gameObject.layer = 0;
		
		}



	}


	public void showFinishButtons(bool value, bool failed){

        if (failed)
        {
            stayBtn.SetActive(value);
            nextBtn.SetActive(false);
            stayBtn.GetComponent<RectTransform>().anchorMin = new Vector2(0.4f, 0.05f);
            stayBtn.GetComponent<RectTransform>().anchorMax = new Vector2(0.6f, 0.20f);
           

        }
        else {
            nextBtn.SetActive(value);
            stayBtn.SetActive(value);
            stayBtn.GetComponent<RectTransform>().anchorMin = new Vector2(0.3f, 0.05f);
            stayBtn.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.20f);
        }
       

	}

    






	public void setupInventoryTiles ()
	{
		string[] inventoryData = ReadLevelText(LevelManager.Instance.pickedAct.idCode, "levelInv", LevelManager.Instance.chosenLevel);

		invSlots = inventoryData.Length;

		for (int i = 0; i < 4; i++) {

			if (invSlots > i) {
				PlaceInventoryTile (i, i, worldStart, inventoryData[i].Trim());
			} else {
				PlaceInventoryTile (i, i, worldStart, null);
			}

		}
		
	}

	public void setupBuildTiles(){





		int shroomCount = ShroomList.Count;
		int yVal = 0;

		buildTileList = new GameObject[shroomCount];

	
		foreach(KeyValuePair<char, GameObject> shroom in ShroomList){

			if (yVal < shroomCount) {

				placeBuildTile (0, yVal, worldStart, shroom.Value, shroomCount, shroom.Key, shroomCount);

			}

			yVal++;


		}

	
	}

	private void placeBuildTile(int x, int y, Vector3 worldStart, GameObject shroom, int rowSize, char key, int shroomCount){
	
		InventoryTile newTile;
		newTile = Instantiate(placerTile, canvas.transform).GetComponent<InventoryTile>();

		float squareHeight = 1.0f / shroomCount;


		newTile.gameObject.GetComponentInChildren<RectTransform> ().anchorMin = new Vector2 (0, y*squareHeight);
		newTile.gameObject.GetComponentInChildren<RectTransform> ().anchorMax = new Vector2 (squareHeight, (y+1)*squareHeight);


		buildTileList[y] = newTile.gameObject;


		newTile.AssignShroom (shroom, 100, key, shroomCount);

	
		if (x == 0 && y == 0) {

				GameManager.Instance.PickTower (newTile);
			}
			
	}





	private void PlaceTile(int x, int y, Vector3 worldStart, char type){
	
		Tile newTile;



		if ((y - x) % 2 == 0) {
			newTile = Instantiate(tile).GetComponent<Tile>();
		} else {
			newTile = Instantiate(tile2).GetComponent<Tile>();
		}

		float width;
		width = (float)(Camera.main.orthographicSize * 2.0)/rowCount;
		float extraYval = (float)Camera.main.orthographicSize * 2.0f * Camera.main.rect.y;


		//newTile.Setup (new Point (x, y),  new Vector3 (centeringValue + worldStart.x + (width * x), worldStart.y - (width * y), 1), new Vector3 (width, width, 1.0f));
		newTile.Setup (new Point (x, y),  new Vector3 (Camera.main.rect.x + centeringValue + worldStart.x + (width * x), worldStart.y - (width * y), 1), new Vector3 (width, width, 1.0f));



		if (type != 'O') {

			newTile.assignDefaultShroom (ShroomList[type], type);


        }



	
	}



	private string[] ReadLevelText(string act, string folder, string lvlNumber)
	{

		Debug.Log ("acts/" + act +"/" +  folder + "/level" + lvlNumber);

		TextAsset bindData = Resources.Load ("acts/" + act +"/" +  folder + "/level" + lvlNumber) as TextAsset;

        currentLevelTextPrefab.GetComponent<Text>().fontSize = Screen.height/22;

        if (lvlNumber.Contains("tutorial")) { currentLevelTextPrefab.GetComponent<Text>().text = act + "- Tutorial"; }
        else currentLevelTextPrefab.GetComponent<Text>().text = act + " - level " + lvlNumber;


        string data = bindData.text.Replace (Environment.NewLine, string.Empty);

		return data.Split ('-');


	}

	public void backToMainMenu ()
	{
		if (!LevelManager.Instance.blockClicks) {
		
			SceneManager.LoadScene ("levelMenu");
		
		}



		
	}

	void setupLevelReqs ()
	{
		
		scoringData = ReadLevelText(LevelManager.Instance.pickedAct.idCode, "levelGoals", LevelManager.Instance.chosenLevel);
		//Color[] buttonColorList = new Color[5]{new Color32(111, 111, 111, 255), new Color32(118, 152, 102, 255), new Color32(73, 92, 0, 255), new Color32(27, 79, 24, 255), new Color32(101, 19, 19, 255)};





		//bronzeTile.GetComponent<Image> ().color = buttonColorList [1];
		//silverTile.GetComponent<Image> ().color = buttonColorList [2];
		//goldTile.GetComponent<Image> ().color = buttonColorList [3];

		if (LevelManager.Instance.editMode) {

			goldTile.GetComponentInChildren<InputField> ().interactable = true;
			silverTile.GetComponentInChildren<InputField> ().interactable = true;
			bronzeTile.GetComponentInChildren<InputField> ().interactable = true;

			bronzeTile.GetComponentInChildren<InputField> ().onValueChanged.AddListener (delegate {
				updateScoreReq (0, bronzeTile.GetComponentInChildren<InputField>().text+"-");
			});
			silverTile.GetComponentInChildren<InputField> ().onValueChanged.AddListener(delegate {
				updateScoreReq (1, silverTile.GetComponentInChildren<InputField>().text+"-");

			});
			goldTile.GetComponentInChildren<InputField> ().onValueChanged.AddListener (delegate {
				updateScoreReq (2, goldTile.GetComponentInChildren<InputField>().text);

			});

		}


		bronzeTile.GetComponentInChildren<InputField>().GetComponentInChildren<Text>().text = scoringData[0];
		silverTile.GetComponentInChildren<InputField>().GetComponentInChildren<Text>().text = scoringData[1];
		goldTile.GetComponentInChildren<InputField>().GetComponentInChildren<Text>().text = scoringData[2];

	}

	public double getScoringData(int i){

		return double.Parse(scoringData [i]);
	
	
	}

	void updateScoreReq (int i, string text)
	{

		String path = Application.dataPath + ("/Resources/acts/" + LevelManager.Instance.pickedAct.idCode + "/levelGoals/level" + LevelManager.Instance.chosenLevel + ".txt");


		string[] arrLine = File.ReadAllLines(path);
		arrLine[i] = text;
		File.WriteAllLines(path, arrLine);

	}    

	public void saveLevelStructure (){
	
		String path = Application.dataPath + ("/Resources/acts/" + LevelManager.Instance.pickedAct.idCode + "/levelStructures/level" + LevelManager.Instance.chosenLevel + ".txt");


		String[] levelLines = new string[rowCount];

		for (int i = 0; i < rowCount; i++) {
		
			StringBuilder lvlLine = new StringBuilder();


		
			for (int j = 0; j < colCount; j++) {
			
				lvlLine.Append (Tiles [new Point (j, i)].ShroomKey);

			}

			if (i != rowCount - 1) {
			
				lvlLine.Append ("-");

			}

			levelLines [i] = lvlLine.ToString ();

		
		}

		File.WriteAllLines(path, levelLines);
	
	
	}

	public void poof ()
	{
		foreach (Point pos in poofers) {
		
			Tiles [pos].GetComponentInChildren<PoofAble> ().Poof();
		
		}
	}

	public void resetPoof ()
	{
		foreach (Point pos in poofers) {

			if (Tiles [pos].GetComponentInChildren<PoofAble> ().status ()) {
				Tiles [pos].GetComponentInChildren<PoofAble> ().Poof ();
			}

		}
		if (hasBait) {
			Tiles [baitPos].GetComponentInChildren<SweetScript> ().resetAnimation();
		}
	}

	public bool InBounds(Point posToCheck){
	
		return (0 <= posToCheck.X && posToCheck.X < colCount) && (0 <= posToCheck.Y && posToCheck.Y < rowCount);

	
	}


    // Update is called once per frame
    void Update () {
		
	}
}
