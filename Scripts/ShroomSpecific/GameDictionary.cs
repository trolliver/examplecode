﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDictionary : MonoBehaviour {

	[System.Serializable]
	public class CharShroomEntry
	{
		public char key;
		public GameObject storedObject;
	}
		
	public CharShroomEntry[] charObject;  


	// Use this for initialization
	void Start () {
		
	}


	public Dictionary<char, GameObject> getDictionary(){

		Dictionary<char, GameObject> shroomList = new Dictionary<char, GameObject>();

		foreach (CharShroomEntry entry in charObject) {
			if(entry.key != char.MinValue)
				shroomList.Add (entry.key, entry.storedObject);
			}

       
		return shroomList;


}
}
	
	

