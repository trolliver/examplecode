﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HoneyScript : Bait, ShroomSpecific {

	// Use this for initialization
	void Awake () {

		if (this.transform.parent.gameObject.GetComponent<Tile> () != null) {

			loadLevel.Instance.addBait2 (this.transform.parent.gameObject.GetComponent<Tile> ().GridPos);


		}

	}

	#region ShroomSpecific implementation
	public void removeShroom ()
	{
		if (loadLevel.Instance != null) {

			loadLevel.Instance.removeBait2 ();

		}
	}
	#endregion

	#region implemented abstract members of Bait

	public override void collect ()
	{
		Active = false;
		loadLevel.Instance.poof ();
	}

	#endregion

	#region implemented abstract members of Bait

	public override void dodge ()
	{
		
	}

	#endregion

	void OnDestroy() {




	}





	// Update is called once per frame
	void Update () {

	}
}

