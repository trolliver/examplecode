﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startTile : MonoBehaviour {

	// Use this for initialization
	void Awake () {

		if (this.transform.parent.gameObject.GetComponent<Tile> () != null) {

			loadLevel.Instance.setStart (this.transform.parent.gameObject.GetComponent<Tile> ().GridPos);

		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
