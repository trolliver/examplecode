﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaiterScript : MonoBehaviour, ShroomSpecific, PoofAble {

	private bool poofed;

	[SerializeField]
	private Sprite afterPoof;

	private Sprite beforePoof;

    private Animator anim;
    // Use this for initialization
    void Awake () {

         anim = GetComponent<Animator>();

        float rnd = Random.Range(1, 100) / 100f;
        Debug.Log(rnd + "random value");

        anim.SetFloat("offset", rnd);


        if (this.transform.parent.gameObject.GetComponent<Tile>() != null)
        {

            loadLevel.Instance.addPoofer(this.transform.parent.gameObject.GetComponent<Tile>().GridPos);

            beforePoof = this.gameObject.GetComponent<SpriteRenderer>().sprite;


        }
        else if (transform.parent.parent.gameObject.GetComponent<Tile>() != null)
        {
            loadLevel.Instance.addPoofer(this.transform.parent.parent.gameObject.GetComponent<Tile>().GridPos);

            beforePoof = this.gameObject.GetComponent<SpriteRenderer>().sprite;
        }



	}

	#region ShroomSpecific implementation
	public void removeShroom ()
	{
		if (loadLevel.Instance != null && this.transform.parent.parent.gameObject.GetComponent<Tile> () != null) {
			loadLevel.Instance.removePoofer (this.transform.parent.parent.gameObject.GetComponent<Tile> ().GridPos);

		}
	}
	#endregion

	void OnDestroy() {


	}

	public void Poof ()
	{
        
		if (!poofed) {

			//gameObject.GetComponent<SpriteRenderer> ().sprite = afterPoof;
            anim.Play("poofer");
            gameObject.GetComponent<SpriteRenderer> ().sortingOrder += 6;
			poofed = true;
		} 
		else {
			gameObject.GetComponent<SpriteRenderer> ().sortingOrder -= 6;
            anim.Play("waiterSleep");
            //gameObject.GetComponent<SpriteRenderer> ().sprite = beforePoof;	
			poofed = false;

		}
	}

	public bool status ()
	{
		return poofed;
	}


	public void pathPoof(){

		gameObject.GetComponent<shroomInfo> ().passable = !gameObject.GetComponent<shroomInfo> ().passable;
		gameObject.transform.parent.GetComponentInParent<Tile> ().setPassable (gameObject.GetComponent<shroomInfo> ().passable);

	}	

	public void reset(){

		gameObject.GetComponent<shroomInfo> ().passable = true;
		gameObject.transform.parent.GetComponentInParent<Tile> ().setPassable (gameObject.GetComponent<shroomInfo> ().passable);

	}	





	// Update is called once per frame
	void Update () {

	}
}
