﻿using System;
using UnityEngine;


public abstract class  Bait : MonoBehaviour
	{

	private bool active;


	public bool Active {
		get {
			return active;
		}
		set {
			active = value;
		}
	}


	public void OnTriggerEnter2D(Collider2D other){

		if (this.transform.parent.gameObject.GetComponent<Tile> () != null) {

			if (active) {

				//collect ();
				//other.GetComponent<Bug> ().sneezeSlow ();

			} else {
			
				//dodge ();
			
			}
		}


	}

	public abstract void collect ();

	public abstract void dodge ();





	}


