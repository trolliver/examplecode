﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SweetScript : Bait , ShroomSpecific
{

	Animator animator;

    private int savedOrderLayer;

	// Use this for initialization
	void Awake () {



        if (this.transform.parent.gameObject.GetComponent<Tile>() != null)
        {

            loadLevel.Instance.addBait(this.transform.parent.gameObject.GetComponent<Tile>().GridPos);
            
            animator = GetComponent<Animator>();


        }
        else if (transform.parent.parent.gameObject.GetComponent<Tile>() != null) {
            loadLevel.Instance.addBait(this.transform.parent.parent.gameObject.GetComponent<Tile>().GridPos);
            
            animator = GetComponent<Animator>();
        }

	}

	#region implemented abstract members of Bait
	public override void collect ()
	{
		Active = false;
		loadLevel.Instance.ActivateBait2 ();
		//loadLevel.Instance.poof ();

	}
	#endregion

	#region implemented abstract members of Bait

	public override void dodge ()
	{
		if(Active){
            if(GetComponent<SpriteRenderer>().sortingOrder>0) savedOrderLayer = GetComponent<SpriteRenderer>().sortingOrder;

            loadLevel.Instance.poof();
            animator.Play ("baitHide");
            
            GetComponent<SpriteRenderer>().sortingOrder = 0;
		}
		

	}

	#endregion

	public void resetAnimation(){
        

		animator.Play ("idle");
        GetComponent<SpriteRenderer>().sortingOrder = savedOrderLayer;


    }

	public void removeShroom ()
	{
		if (loadLevel.Instance != null) {
			loadLevel.Instance.removeBait ();

		}
	}


	void OnDestroy() {
			


		}

        
		

	
	// Update is called once per frame
	void Update () {
		
	}
}
