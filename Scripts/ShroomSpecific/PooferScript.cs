﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooferScript : MonoBehaviour, ShroomSpecific, PoofAble  {

	private bool poofed;

    [SerializeField]
    private GameObject text;

    [SerializeField]
    private GameObject smoke;

    Animator animator;

    int baseOrder;

    // Use this for initialization
    void Awake () {


        if (this.transform.parent.gameObject.GetComponent<Tile>() != null)
        {

            loadLevel.Instance.addPoofer(this.transform.parent.gameObject.GetComponent<Tile>().GridPos);

            animator = GetComponent<Animator>();


        }
        else if (transform.parent.parent.gameObject.GetComponent<Tile>() != null)
        {
            loadLevel.Instance.addPoofer(this.transform.parent.parent.gameObject.GetComponent<Tile>().GridPos);

            animator = GetComponent<Animator>();
        }

     

	}

	#region ShroomSpecific implementation
	public void removeShroom ()
	{
		if (loadLevel.Instance != null && this.transform.parent.parent.gameObject.GetComponent<Tile> () != null) {
			loadLevel.Instance.removePoofer (this.transform.parent.parent.gameObject.GetComponent<Tile> ().GridPos);

		}
	}
	#endregion

	public bool status ()
	{
		return poofed;
	}



	public void Poof ()
	{
		if (!poofed) {

            baseOrder = GetComponent<SpriteRenderer>().sortingOrder;
            text.GetComponent<SpriteRenderer>().sortingOrder = baseOrder + 2;
            smoke.GetComponent<SpriteRenderer>().sortingOrder = baseOrder + 1;
            animator.Play("poofer");
            GetComponent<SpriteRenderer>().sortingOrder = 0;
            poofed = true;
		} 
		else {
            GetComponent<SpriteRenderer>().sortingOrder = baseOrder;
            animator.Play("idle");
			poofed = false;

		}

	}

	public void OnDestroy() {



	}

	public void pathPoof(){
	
		gameObject.GetComponent<shroomInfo> ().passable = !gameObject.GetComponent<shroomInfo> ().passable;
		transform.parent.GetComponentInParent<Tile> ().setPassable (gameObject.GetComponent<shroomInfo> ().passable);
	
	}	

	public void reset(){

		gameObject.GetComponent<shroomInfo> ().passable = false;
		transform.parent.GetComponentInParent<Tile> ().setPassable (gameObject.GetComponent<shroomInfo> ().passable);

	}	





	// Update is called once per frame
	void Update () {

	}
}
