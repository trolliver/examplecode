﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZneezerScript : MonoBehaviour {


	[SerializeField]
	private double coolDown;

	[SerializeField]
	private double duration;


	Animator animator;



	private double coolDownTimer;

	private double bugSpeed;


	private Collider2D bug;

	private bool onCoolDown;


	// Use this for initialization
	void Awake () {


		if (this.transform.parent.gameObject.GetComponent<Tile> () != null) {

			loadLevel.Instance.addZneezer(this.transform.parent.gameObject.GetComponent<Tile> ().GridPos);

			//this.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");

			coolDownTimer = 0;
			animator = GetComponent<Animator> ();





			}

	}

	public void OnTriggerEnter2D(Collider2D other){
	
		if (this.transform.parent.gameObject.GetComponent<Tile> () != null) {

			if (!onCoolDown) {

				//other.GetComponent<Bug> ().sneezeSlow ();

			}		
		}

	
	}

	public void slowBug(){
					bugSpeed = bug.GetComponent<Bug> ().Speed;
					bug.GetComponent<Bug> ().sneezeSlow (duration);
					onCoolDown = true;
	}

	void OnTriggerStay2D(Collider2D other) {
		if (other.GetComponent<Bug>() && this.transform.parent.gameObject.GetComponent<Tile> () != null) {
		
			if (!onCoolDown) {
			
				bug = other;
				animator.Play ("Sneeze");
			
			}		

		
		}
			

	}


	void OnDestroy() {

		if (loadLevel.Instance != null && this.transform.parent.gameObject.GetComponent<Tile> () != null) {
			loadLevel.Instance.removeZneezer (this.transform.parent.gameObject.GetComponent<Tile> ().GridPos);

		}

	}







	// Update is called once per frame
	void FixedUpdate () {

		if (onCoolDown) {
		
			coolDownTimer += Time.deltaTime*bugSpeed;

			if (coolDownTimer > coolDown) {
			
				onCoolDown = false;
				coolDownTimer = 0;

			}

		
		}

	}
}