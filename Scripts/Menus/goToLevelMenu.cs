﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class goToLevelMenu : MonoBehaviour {

    private void Awake()
    {
        SaveLoad.Load();
    }

    void Start(){

		Screen.orientation = ScreenOrientation.LandscapeLeft;

	}

	// Use this for initialization
	public void playModeActMenu () {


		LevelManager.Instance.editMode = false;
		SceneManager.LoadScene ("actMenu");


	}

	public void editModeActMenu () {


		LevelManager.Instance.editMode = true;
		SceneManager.LoadScene ("actMenu");


	}

}
