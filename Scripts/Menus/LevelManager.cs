﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class LevelManager : Singleton<LevelManager> {

	public string chosenAct { get; private set; }
	public string chosenLevel { get; private set; }
	public actTile pickedAct { get; private set; }

	public bool editMode { get; set; }

	public bool blockClicks { get; set; }



	void Awake(){
	
		DontDestroyOnLoad (LevelManager.Instance);

	
	}


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void PickLevel(string level){

		this.chosenLevel = level;

	}

	public void PickAct(string act){

		this.chosenAct = act;


	}

	public void chooseAct(actTile act){

		this.pickedAct = act;


	}

    



}

