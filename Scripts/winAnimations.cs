﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class winAnimations : MonoBehaviour {


	[SerializeField]
	private GameObject[] animationControllers;


	private Animator animator;

	public void playAnimation(int rank){


		animator = Instantiate (animationControllers [rank].GetComponent<Animator> ());
		animator.Play("winEnter");
	
	}


	public void stopAnimation(){
	
		if (animator!=null) {
			animator.SetBool ("done", true);
		}

		 
	
	}


}
