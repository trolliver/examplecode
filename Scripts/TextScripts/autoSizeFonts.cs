﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class autoSizeFonts : MonoBehaviour {

	public bool smaller;

	public bool stopOutLine;

	public bool tutorialText;
	public bool invTile;



	void Start () {

		float percentageValue = GetComponent<RectTransform> ().anchorMax.y - GetComponent<RectTransform> ().anchorMin.y;
        
        float fontSize;


		float scaler = Camera.main.GetComponent<Camera> ().rect.height;



        RectTransform rect = GetComponent<RectTransform>();
        


		if (smaller) {

			fontSize = percentageValue * Screen.height * 0.75f * scaler;





		} else if (tutorialText) {
		

			fontSize = percentageValue * Screen.height * 0.35f * scaler;
		
		}
        else if (invTile)
        {


            fontSize = percentageValue * Screen.height * 0.5f * scaler;

        }

        else {
			fontSize = percentageValue * Screen.height * 0.9f * scaler;
		
	}

		if (LevelManager.Instance.editMode) {
			fontSize = fontSize*0.5f;
		}
			

		foreach (Text txt in GetComponentsInChildren<Text>()) {


		
			txt.fontSize = Mathf.RoundToInt(fontSize);
            


			if (!stopOutLine) {
			
				float outlineText = fontSize / 60;



				Outline text = txt.gameObject.AddComponent<Outline> ();

				int boost = 255;

				Color outLineClr = new Color (0,0,0,255);
				text.effectColor = outLineClr;

				text.effectDistance = new Vector2 (outlineText, -outlineText);

			
			}

				
		

		
		}
		
	}
	

}
